<?php
/**
 * Plugin Name: Tangible: Loops for Gravity Forms
 * Plugin URI: https://tangibleplugins.com/tangible-loops-for-gravity-forms
 * Description: Loop types, fields, and conditions for Gravity Forms
 * Version: 0.0.5
 * Author: Team Tangible
 * Author URI: https://teamtangible.com
 * License: GPLv2 or later
 */

define( 'TANGIBLE_LOOPS_FOR_GRAVITY_FORMS_VERSION', '0.0.5' );

require __DIR__ . '/vendor/tangible/plugin-framework/index.php';

/**
 * Get plugin instance
 */
function tangible_loops_for_gravity_forms($instance = false) {
  static $plugin;
  return $plugin ? $plugin : ($plugin = $instance);
}

add_action('plugins_loaded', function() {

  $framework = tangible();
  $plugin    = $framework->register_plugin([
    'name'           => 'tangible-loops-for-gravity-forms',
    'title'          => 'Loops for Gravity Forms',
    'setting_prefix' => 'tangible_loops_for_gravity_forms',

    'version'        => TANGIBLE_LOOPS_FOR_GRAVITY_FORMS_VERSION,
    'file_path'      => __FILE__,
    'base_path'      => plugin_basename( __FILE__ ),
    'dir_path'       => plugin_dir_path( __FILE__ ),
    'url'            => plugins_url( '/', __FILE__ ),
    'assets_url'     => plugins_url( '/assets', __FILE__ ),

    // Product ID on Tangible Plugins store - See also package.json, property "productId"
    'item_id'        => 20197,
    'multisite'      => false,
  ]);

  $plugin->register_dependencies([
    'tangible-loops-and-logic/tangible-loops-and-logic.php' => [
      'title' => 'Tangible Loops & Logic',
      'url' => 'https://tangibleplugins.com/tangible-loops-and-logic',
      'fallback_check' => function() {
        return function_exists('tangible_loops_and_logic');
      }
    ],
    'gravityforms/gravityforms.php' => [
      'title' => 'Gravity Forms',
      'url' => 'https://www.gravityforms.com/',
      'fallback_check' => function() {
        return class_exists('GFForms');
      }
    ],
  ]);

  tangible_loops_for_gravity_forms( $plugin );

  // Depends on Loops & Logic
  add_action('tangible_loops_and_logic_ready', function($loop, $logic, $html) use ($framework, $plugin) {

    if ( ! $plugin->has_all_dependencies() ) return;

    // Features loaded will have in their scope: $framework, $plugin, ..

    require_once __DIR__.'/includes/index.php';

  }, 10, 3);

}, 10);
