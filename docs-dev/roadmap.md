# Roadmap

- [x] **Entry**
    - [x] **Add arguments and fields**

    - [x] **Filter by**:
        - [x] Form ID(s)
        - [x] Created by User ID 
        - [x] Start / End date of creation
        - [x] Is the Entry starred
        - [x] Is the Entry read
        - [x] Page size(number)/offset
      
    - [x] **Order by**:
        - [x] Order by one of: "id", "form_id", "date_created", "is_starred", "currency",	"payment_status","payment_date", "payment_amount",	"payment_method", "transaction_id", "is_fulfilled", "created_by", "transaction_type", "status"

- [x] **Entry Fields**
    - [x] **Add arguments and fields**
    
##Left for later:
- [ ] Extend content types with more GravityForms [AddOns]( https://www.gravityforms.com/add-ons/ )

