<?php

  $html->add_closed_tag('GField', function($atts,$nodes) use($plugin,$loop,$html){

    $content = '';

    $field_id = isset($atts['id']) ? $atts['id'] : '';

    if( isset($atts['entry_id']) && !empty($atts['entry_id']) ){
      $entry_id = trim($atts['entry_id']);
    } else{
      $current_loop = $loop->get_current();
      $entry_id = $current_loop -> current;
    }

    $value_key = isset($atts['value_key']) ? trim($atts['value_key']) : '';

    if( empty($field_id) || empty($entry_id) ) return $content;

    $wrap = false;

    $entry = \GFAPI::get_entry( $entry_id );
    $form_id = $entry['form_id'];
    $form = \GFAPI::get_form( $form_id );
    $field = \GFAPI::get_field( $form, $field_id );
    if(empty($field))  return $content;

    $value = \RGFormsModel::get_lead_field_value( $entry, $field );


    if( in_array('label', $atts['keys']) )  return $field -> label;
    if( in_array('type', $atts['keys']) )  return $field -> type;
    if( in_array('input_type', $atts['keys']) )  return $field -> inputType;

    $type = $field -> type;

    // survey, quiz, poll add ons
    $addon = '';
    if($field -> type == 'survey' || $field -> type == 'quiz' || $field -> type == 'poll' ) {
      $type = $field -> inputType;
      $addon = $field -> type;
    }

    switch ($type) {
      case 'text':
      case 'textarea':
      case 'number':
      case 'time':
      case 'date':
      case 'email':
      case 'phone':
      case 'website':
      case 'post_title':
      case 'post_excerpt':
      case 'post_content':
      case 'post_tags':
      case 'quantity':
      case 'total':
      $display_value =  $value;
        $content = $display_value;
        break;

      case 'select':
      case 'checkbox':
      case 'multiselect':
      case 'radio':
        $display_value =  $plugin -> get_choice_type_field_values( $value, $field, $form_id);
        if($type=='select' || $type=='radio'){
          if(empty($value_key)) $value_key = array_keys($display_value[0])[0];
          $content = isset($display_value[0]) ? $display_value[0][$value_key] : '';
        }else{
          $content = $display_value;
          if($field -> type == 'poll' && $type=='checkbox' && $value_key=='results') $content = $display_value[0]['results'];
        }
        break;

      case 'name':
        $display_value =  $plugin -> get_name_field_values( $value, $field, $form_id);
        if(empty($value_key)) $value_key = array_keys($display_value)[0];
        $content = $display_value[$value_key];
        break;

      case 'address':
        $display_value =  $plugin -> get_address_field_values( $value, $field, $form_id);
        if(empty($value_key)) $value_key = array_keys($display_value)[0];
        $content = $display_value[$value_key];
        break;

      case 'consent':
        $display_value =  $plugin -> get_consent_field_values( $value, $field );
        if(empty($value_key)) $value_key = array_keys($display_value)[0];
        $content = $display_value[$value_key];
        break;

      case 'fileupload':
        $display_value =  $plugin -> get_fileupload_field_values( $value, $field );
        $content = $display_value;
        break;

      case 'list':
        $display_value = \GFCommon::get_lead_field_display( $field, $value, $entry['currency'] );
        $content = $display_value;
        break;

      case 'likert':
        $display_value =  $plugin -> get_survey_field_values( $value, $field, 'likert');
        $content = $display_value;
        break;

      case 'rank':
        $display_value =  $plugin -> get_survey_field_values( $value, $field, 'rank');
        $content = $display_value;
        break;

      case 'rating':
        if(empty($value_key)) $value_key='text';
        $display_value =  $plugin -> get_survey_field_values( $value, $field, 'rating');
        if(empty($value_key)) $value_key = array_keys($display_value[0])[0];
        $content = $display_value[0][$value_key];
        break;

      case 'post_category':
        $display_value =  $plugin -> get_post_taxonomy_field_values( $value, $field);
        $content = $display_value;
        break;

      case 'post_image':
        $display_value =  $plugin -> get_post_image_field_values( $value, $field);
        if(empty($value_key)) $value_key = array_keys($display_value)[0];
        $content = $display_value[$value_key];
        break;

      case 'product':
        $display_value =  $plugin -> get_product_field_values( $value, $field);
        if(empty($value_key)) $value_key = array_keys($display_value)[0];
        $content = $display_value[$value_key];
        break;

      case 'option':
        $display_value =  $plugin -> get_option_field_values( $value, $field);
        $content = $display_value;
        break;

        case 'shipping':
        $display_value =  $plugin -> get_shipping_field_values( $value, $field);
          if(empty($value_key)) $value_key = array_keys($display_value)[0];
          $content = $display_value[$value_key];
        break;

      case 'content':
      default:
        break;
    }

    if (!$wrap || empty($content)) return $content;

    return '<div class="tgbl-loops-for-gravity-forms">'.$content.'</div>';
  });
