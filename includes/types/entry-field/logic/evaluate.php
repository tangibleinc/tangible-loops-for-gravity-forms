<?php
  use BetterLifter as lift;

/**
 * Evaluate
 *
 * @see tangible-loops-and-logic/vendor/tangible/template/logic/evaluate.php
 */
return $plugin->evaluate_course_logic_rule = function($rule) use ($plugin, $loop, $logic, $html) {

  $condition = true;

  $field = @$rule['field'];
  $compare = @$rule['operand'];
  $value = @$rule['value'];

  $current_loop = $loop->get_current('lifter_course');

  switch ($field) {
    case 'course':
      switch ($compare) {
        case 'is':
        case 'is_not':

          switch ($value) {
            case 'enrolled':
              $condition = lift\user_course_is_enrolled();
              if ($compare==='is_not') $condition = !$condition;
              break;
            case 'completed':
              $condition = lift\has_complete_course();
              if ($compare==='is_not') $condition = !$condition;
              break;
            case 'started':
              $condition = lift\has_started_course();
              if ($compare==='is_not') $condition = !$condition;
              break;
          }
          break;
      }
      break;
    case 'course_progress':
      $percentage = lift\course_progress_percent_int();
      switch ($value) {
        case 'greater_than':
          $condition = $percentage > $value;
          break;
        case 'greater_than_or_equal':
          $condition = $percentage >= $value;
          break;
        case 'less_than':
          $condition = $percentage < $value;
          break;
        case 'less_than_or_equal':
          $condition = $percentage <= $value;
          break;
      }
      break;

    case 'certificate':
      switch ($value) {
        case 'earned':
          $user_id  = get_current_user_id();
          $student = llms_get_student( $user_id );
          $course_id = lift\course_id();

          if( empty($student) || empty($course_id) ) return false;

          $certificate_earned = llms_get_user_postmeta( $student->get('id'), $course_id , '_certificate_earned', true, 'meta_value' );
          $c = !empty($certificate_earned);
          $condition = $compare === 'is_not' ? !$c : $c;
          break;
      }
      break;
  }

  return $condition;
};

