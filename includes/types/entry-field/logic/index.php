<?php
  use BetterLifter as lift;
/**
 * Register with Logic module
 */

$logic->extend_rules_by_category(
  'lifterlms',
  require_once __DIR__.'/rules.php',
  require_once __DIR__.'/evaluate.php'
);
