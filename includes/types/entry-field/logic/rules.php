<?php
  /**
   * Rule definitions
   *
   */
  return $plugin->course_logic_rules = [
    // enrolled
    [ 'name' => 'course',
      'label' => 'LifterLMS Course',
      'operands' => [
        [ 'name' => 'is', 'label' => 'is' ],
        [ 'name' => 'is_not', 'label' => 'is not' ],
      ],
      'values' => [
        'type' => 'select',
        'options' => [
          [ 'name' => 'enrolled', 'label' => 'Enrolled' ],
          [ 'name' => 'started', 'label' => 'Started' ],
          [ 'name' => 'completed', 'label' => 'Completed' ],
        ],
      ],
    ],
    // progress
    [ 'name' => 'course_progress',
      'label' => 'LifterLMS Course progress',
      'operands' => [
        [ 'name' => 'greater_than', 'label' => 'is greater than' ],
        [ 'name' => 'greater_than_or_equal', 'label' => 'is greater than or equal to' ],
        [ 'name' => 'less_than', 'label' => 'is less than' ],
        [ 'name' => 'less_than_or_equal', 'label' => 'is less than or equal to' ],
      ],
      'values' => [
        [ 'type' => 'number', 'unit' => '%',
          'min' => 0, 'max' => 100,
        ]
      ],
    ],
    [ 'name' => 'certificate',
      'label' => 'LifterLMS Course Certificate',
      'operands' => [
        [ 'name' => 'is', 'label' => 'is' ],
        [ 'name' => 'is_not', 'label' => 'is not' ],
      ],
      'values' => [
        [ 'name' => 'earned', 'label' => 'earned',
        ]
      ],
    ],
  ];

