  <?php
  defined('ABSPATH') or die();

  /**
   * Get entry field ids
   *
   * @param array   $args    arguments to pass
   *
   * @return arr $arr field id's
   */
  $plugin -> get_entry_fields = function( $args = []) use ($plugin) {
    $entry_id = isset($args['entry_id']) ? $args['entry_id'] : '';
    if(empty($entry_id)) return [];

    $entry = \GFAPI::get_entry( $entry_id );
    $form_id = $entry['form_id'];
    $form = \GFAPI::get_form( $form_id );

    $fields =[];
    foreach ( $form['fields'] as $field ) {

      switch ( $field->get_input_type() ) {
        case 'captcha':
        case 'html':
        case 'password':
        case 'page':
          // Ignore captcha, html, password, page field.
          break;
        default :
          $fields[] = $field -> id;
          break;
      }
    }


    return $fields;
  };


  /**
   * Get consent field display values
   *
   * @param array   $value  raw value to process
   * @param object  $field  field object
   *
   * @return array  field display values
   */
  $plugin -> get_consent_field_values = function($value, $field) use ($plugin) {
    $consent_field_values =
      [
        'text' => '',
        'description' => '',
      ];
    if(empty($value) || empty($field)) return $consent_field_values;

    $description = '';
    if ( is_array( $value ) ) {
      $consent     = trim( $value[ $field->id . '.1' ] );
      $text        = trim( $value[ $field->id . '.2' ] );
      $revision_id = absint( trim( $value[ $field->id . '.3' ] ) );

      if ( ! rgblank( $consent ) ) {
        $text = wp_kses_post( $text );
        // checking revisions.
        $description = nl2br( $field->get_field_description_from_revision( $revision_id ));
      }
    }

    $consent_field_values =
      [
        'text' => $text,
        'description' => $description,
      ];

    return $consent_field_values;
  };


  /**
   * Get fileupload field display values
   *
   * @param mixed   $value  raw value to process
   * @param object  $field  field object
   *
   * @return array  field display values
   */
  $plugin -> get_fileupload_field_values = function($value, $field) use ($plugin) {

    if(empty($value) || empty($field)) return [];

    $fileupload_field_values = $field->multipleFiles ? json_decode( $value ) : [ $value ];

    return $fileupload_field_values;
  };


  /**
   * Get product option field display values
   *
   * @param mixxed   $value  raw value to process
   * @param object  $field  field object
   *
   * @return array  field display values
   */
  $plugin -> get_option_field_values = function($value, $field) use ($plugin) {
    $option_field_values = [];
    if(empty($value) || empty($field)) return $option_field_values;

    $value = !is_array( $value ) ? [$value] : $value ;
    $option_name = '';
    $price = '';
    foreach ( $value as $key => $v ) {
      if ( !rgblank( $v ) ) {
        if ( !is_array( $v ) ) {
          $ary   = explode( '|', $v );
          $option_name = $ary[0];
          $price = count( $ary ) > 1 ? $ary[1] : '';
        }
        $option_field_values[]=['name'=>$option_name,'price'=>$price];
      }
    }

    return $option_field_values;
  };


  /**
   * Get address field display values
   *
   * @param array   $value  raw value to process
   * @param object  $field  field object
   *
   * @return array  field display values
   */
  $plugin -> get_address_field_values = function($value, $field) use ($plugin) {
    $address_field_values =
      [
        'street' => '',
        'street2' => '',
        'city' => '',
        'state' => '',
        'zip' => '',
        'country' => '',
      ];
    if(empty($value) || empty($field)) return $address_field_values;

    if ( is_array( $value ) ) {
      $street_value = trim( rgget( $field -> id . '.1', $value ) );
      $street2_value = trim( rgget( $field-> id . '.2', $value ) );
      $city_value = trim( rgget( $field -> id . '.3', $value ) );
      $state_value = trim( rgget( $field -> id . '.4', $value ) );
      $zip_value = trim( rgget( $field -> id . '.5', $value ) );
      $country_value = trim( rgget( $field -> id . '.6', $value ) );

      $address_field_values =
        [
          'street' => $street_value,
          'street2' => $street2_value,
          'city' => $city_value,
          'state' => $state_value,
          'zip' => $zip_value,
          'country' => $country_value,
        ];
    }

    return $address_field_values;
  };


  /**
   * Get name field display values
   *
   * @param array   $value  raw value to process
   * @param object  $field  field object
   *
   * @return array  field display values
   */
  $plugin -> get_name_field_values = function($value, $field) use ($plugin) {
    $name_field_values =
      [
        'prefix' => '',
        'first' => '',
        'middle' => '',
        'last' => '',
        'suffix' => '',
      ];
    if(empty($value) || empty($field)) return $name_field_values;

    if ( is_array( $value ) ) {
      $prefix = trim( rgget( $field ->id . '.2', $value ) );
      $first  = trim( rgget( $field ->id . '.3', $value ) );
      $middle = trim( rgget( $field ->id . '.4', $value ) );
      $last   = trim( rgget( $field ->id . '.6', $value ) );
      $suffix = trim( rgget( $field ->id . '.8', $value ) );

      $name_field_values =
        [
          'prefix' => $prefix,
          'first' => $first,
          'middle' => $middle,
          'last' => $last,
          'suffix' => $suffix,
        ];
    }

    return $name_field_values;
  };


  /**
   * Get post_image field display values
   *
   * @param string   $value  raw value to process
   * @param object  $field  field object
   *
   * @return array  field display values
   */
  $plugin -> get_post_image_field_values = function($value, $field) use ($plugin) {
    $post_image_field_values =
      [
        'url' => '',
        'title' => '',
        'caption' => '',
        'description' => '',
      ];
    if(empty($value) || empty($field)) return $post_image_field_values;

    $ary         = explode( '|:|', $value );
    $url         = count( $ary ) > 0 ? $ary[0] : '';
    $title       = count( $ary ) > 1 ? $ary[1] : '';
    $caption     = count( $ary ) > 2 ? $ary[2] : '';
    $description = count( $ary ) > 3 ? $ary[3] : '';

    $post_image_field_values =
      [
        'url' => $url,
        'title' => $title,
        'caption' => $caption,
        'description' => $description,
      ];

    return $post_image_field_values;
  };


  /**
   * Get post_taxonomy fields(category, tags) display values
   *
   * @param mixxed   $value  raw value to process
   * @param object  $field  field object
   *
   * @return array  field display values
   */
  $plugin -> get_post_taxonomy_field_values = function($value, $field) use ($plugin) {
    $post_category_field_values = [];
    if(empty($value) || empty($field)) return $post_category_field_values;

    if($field -> inputType == 'radio' || $field -> inputType == 'select' || $field -> inputType == 'text' ){
      if ( !is_array( $value ) ) {
        $ary   = explode( ':', $value );
        $cat_name = $ary[0];
        $cat_id = count( $ary ) > 1 ? $ary[1] : '';
      }
      $post_category_field_values[] = ['name'=>$cat_name,'id'=>$cat_id];

    } elseif($field -> inputType == 'checkbox' || $field -> inputType == 'multiselect'){
      if($field -> inputType == 'multiselect') $value = json_decode($value,true); // ["multi First Choice","multi Second Choice","multi Third Choice"]

      $cat_name='';
      $cat_id='';
      foreach ( $value as $key => $v ) {
        if ( !rgblank( $v ) ) {
          if ( !is_array( $v ) ) {
            $ary   = explode( ':', $v );
            $cat_name = $ary[0];
            $cat_id = count( $ary ) > 1 ? $ary[1] : '';
          }
        }
        $post_category_field_values[] = ['name'=>$cat_name,'id'=>$cat_id];
      }
    }

    return $post_category_field_values;
  };


  /**
   * Get shipping field display values
   *
   * @param string   $value  raw value to process
   * @param object  $field  field object
   *
   * @return array  field display values
   */
  $plugin -> get_shipping_field_values = function($value, $field) use ($plugin) {
    $shipping_field_values =
      [
        'name' => '',
        'price' => '',
      ];

    if(empty($value) || empty($field)) return $shipping_field_values;

    if($field -> inputType == 'singleshipping'){
      $shipping_name = esc_attr( $field->label ) ;
      $price        =  $value;
    }

    if($field -> inputType == 'radio' || $field -> inputType == 'select') {
      if ( !is_array( $value ) ) {
        $ary   = explode( '|', $value );
        $shipping_name = $ary[0];
        $price = count( $ary ) > 1 ? $ary[1] : '';
      }
    }

    $shipping_field_values =
      [
        'name' => $shipping_name,
        'price' => $price,
      ];

    return $shipping_field_values;
  };


  /**
   * Get product field display values
   *
   * @param mixxed   $value  raw value to process
   * @param object  $field  field object
   *
   * @return array  field display values
   */
  $plugin -> get_product_field_values = function($value, $field) use ($plugin) {
    $product_field_values =
      [
        'name' => '',
        'price' => '',
        'quantity' => '',
      ];
    if(empty($value) || empty($field)) return $product_field_values;



    if($field -> inputType == 'singleproduct' || $field -> inputType == 'hiddenproduct' || $field -> inputType == 'calculation'){
      $product_name = ! is_array( $value ) || empty( $value[ $field->id . '.1' ] ) ? esc_attr( $field->label ) : esc_attr( $value[ $field->id . '.1' ] );
      $price        = ! is_array( $value ) || empty( $value[ $field->id . '.2' ] ) ? $field->basePrice : esc_attr( $value[ $field->id . '.2' ] );
    }

    if($field -> inputType == 'price') {
      $product_name =  esc_attr( $field->label ) ;
      $price        = empty( $value) ? $field->basePrice : esc_attr( $value );
    }

    if($field -> inputType == 'radio' || $field -> inputType == 'select') {
      if ( !is_array( $value ) ) {
        $ary   = explode( '|', $value );
        $product_name = $ary[0];
        $price = count( $ary ) > 1 ? $ary[1] : '';
      }
    }

    $product_field_values =
      [
        'name' => $product_name,
        'price' => $price,
      ];

    return $product_field_values;
  };


  /**
   * Get choices type(checkbox, radio, select, multiselect) fields display values
   *
   * @param array   $value  raw value to process
   * @param object  $field  field object
   *
   * @return array  field display values
   */
  $plugin -> get_choice_type_field_values = function($value, $field) use ($plugin) {

    $field_values = [];
    if(empty($value) || empty($field)) return $field_values;

    $field_type =$field -> type;

    // survey, quiz, poll add ons
    if($field -> type == 'survey' || $field -> type == 'quiz' || $field -> type == 'poll' ) $field_type = $field -> inputType;

    if($field_type =='multiselect') $value = json_decode($value,true);

    if(!is_array($value)) $value = [$value];

    if($field_type == 'checkbox' || $field_type =='multiselect'|| $field_type =='radio'|| $field_type =='select'){

      $choices = $field -> choices;

      foreach ( $value as $key => $v ) {
        if ( ! rgblank( $v ) ) {

          $text = '';
          foreach($choices as $key => $choice ){
            if( $choice['value'] == $v ){
              $text = $choice['text'];

              // add Quiz specific values: gquizIsCorrect, gquizWeight
              $is_correct = '';
              $weight = '';
              if($field -> type == 'quiz'){
                $is_correct = isset($choice['gquizIsCorrect']) ? $choice['gquizIsCorrect'] : false;
                $is_correct = $is_correct ? 'yes' : 'no';
                $weight = isset($choice['gquizWeight']) ? $choice['gquizWeight'] : 0 ;
              }

              // add Poll specific values: total_entries, ratio(percentage)
              $results = [];
              $poll_active = false;
              if($field -> type == 'poll'){

                // Check that the Poll add on is active
                if ($plugin -> is_addon_active('GFPolls')) {
                  $poll_active = true;
                  $GFPolls = new \GFPolls;
                  $poll_results = $GFPolls -> gpoll_get_results(  $field -> formId, $field -> id );

                  $results = $poll_results['fields'][0]['inputs'];
                  $res =[];
                  foreach($results as $r){
                    $res[] = ['label'=>$r['label'], 'total_entries'=>$r['total_entries'], 'ratio'=>$r['ratio'], ];
                  }
                }
              }
            }
          }
          if(!$poll_active) $res='';
          $field_values[]= ['text' => $text, 'value' => $v, 'is_correct' => $is_correct, 'weight' => $weight, 'results' => $res];
        }
      }
    }
    return $field_values;
  };


  //** Survey add on fields **//

  /**
   * Get survey  field display values
   *
   * @param mixxed   $value  raw value to process
   * @param object  $field  field object
   *
   * @return array  field display values
   */
  $plugin -> get_survey_field_values = function($value, $field, $input_type= '' ) use ($plugin) {
    $survey_field_values = [];
    if(empty($value) || empty($field)  || empty($input_type)) return $survey_field_values;

    if($input_type == 'likert'){
      $columns = $field -> choices;
      $rows = $field -> gsurveyLikertRows;
      $multi_rows = $field -> gsurveyLikertEnableMultipleRows;

      if($multi_rows === false && !is_array($value)){
        $value = [$rows[0]['value'].':'.$value]; /// $value = [ $row_index:$col_index]
      }

      $columns_reindexed=[];
      foreach($columns as $ck => &$c ){
        $columns_reindexed[$c['value']] = $c ;
      }

      $rows_reindexed=[];
      foreach($rows as $rk => &$r ){
        $rows_reindex[$r['value']] = $r ;
      }

      foreach($value as $v){
        if (empty($v)) continue;
        $arry=explode(':', $v);
        $row = $rows_reindex[$arry[0]];
        $col = $columns_reindexed[$arry[1]];

        $survey_field_values[] =['row' =>$row['text'] , 'choice' =>$col['text'] , 'score' =>$col['score'] ];
      }

      return $survey_field_values;
      //return ['$value',$value, '$rows_reindex',$rows_reindex, '$columns_reindex',$columns_reindexed];

    } elseif($input_type == 'rank'){
      $ordered_values = ! empty( $value ) ? explode( ',', $value ) : '';
      $new_value      = '';

      foreach ( $ordered_values as &$ordered_value ) {
        $survey_field_values[] = \RGFormsModel::get_choice_text( $field, $ordered_value );
      }

      return $survey_field_values;

    } elseif($input_type == 'rating'){
      $text = \RGFormsModel::get_choice_text( $field, $value );
      $survey_field_values[] = ['text'=>$text, 'value'=>$value];

      return $survey_field_values;
    }

  };