<?php
  namespace Tangible\Loop\Integrations\GravityForms;

  use Tangible\Loop\BaseLoop;


  class EntryFieldsLoop extends BaseLoop {

    static $loop; // See after class
    static $plugin;// See after class

    static $config = [
      'name'       => 'gravity_form_entry_fields', // Must correspond with $loop( $loop_type, .. )
      'title'      => 'GravityForms Entry Fields',
      'category'    => 'gravityforms',
      'description' => 'Loop through GravityForms Entry fields',

    ];

    function __construct( $args = [] ) {
      parent::__construct( $args );
    }

    function create_query_args( $args = [] ) {
      $query_args = parent::create_query_args( $args );
      return $query_args;
    }


    function run_query( $query_args ) {
      $args = parent::run_query( $query_args);
      $entry_id = isset($args['entry_id']) ? $args['entry_id'] : '';

      return self::$plugin -> get_entry_fields(['entry_id' => $entry_id]);
    }

    function get_items_from_query( $query ) {

      $plugin = self::$plugin;

      $items = parent::get_items_from_query( $query );

      // Filter items

      // field ids
      if ( ! empty($this->args['field_id']) ) {

        $match_types =  $plugin -> string_to_array($this->args['field_id']);

        if( !empty($match_types) ) {
          $items = array_values( array_filter( $items, function ( $item ) use ( $match_types,$plugin ) {
            return in_array($item,$match_types);
          } ) );
        }
      }

      //tgbl()->see('$items',$items);
      return $this->items = $items;
    }

    function get_item_field( $field_id, $field_name, $args = [] ) {

      $prefix = 'field';
      // add prefix to fields, backward compatibility
      $field_name = self::$plugin -> field_prefix_handle( $field_name, $prefix);

      if ( empty($field_id) ) return;

      $entry_id = $this->args['entry_id'];
      $entry = \GFAPI::get_entry( $entry_id );

      $form_id = $entry['form_id'];
      $form = \GFAPI::get_form( $form_id );
      $field = \GFAPI::get_field( $form, $field_id );

      $value = \RGFormsModel::get_lead_field_value( $entry, $field );

      //if($field -> type == 'number'){
        //tgbl() -> see($value);
      //}

      /**
       * Prevent throwing php notice 'GF_Field::get_value_entry_detail was called incorrectly' when the field is 'survey' type but Survey add on plugin deactivated
       *
       */
      if(!self::$plugin -> is_addon_active('GFSurvey') && is_array($value)){
        $display_value = '';
      } else{
        $display_value = \GFCommon::get_lead_field_display( $field, $value, $entry['currency'] );
      }

      $label = esc_html( \GFCommon::get_label( $field ) );

      /**
       * STANDARD fields
       */

      if($field -> type == 'checkbox' || $field -> type =='multiselect'|| $field -> type =='radio'|| $field -> type =='select') {
        $display_value =  self::$plugin -> get_choice_type_field_values( $value, $field, $form_id);
      }

      /**
       * ADVANCED fields
       */

      if($field -> type =='email' ||$field -> type =='website' ) {
        $display_value = $value;
      }

      if($field -> type =='fileupload') {
        $fileupload_field_values = self::$plugin -> get_fileupload_field_values($value,$field);
        $display_value = $fileupload_field_values;
      }

      if($field -> type =='name') {
        $name_field_values = self::$plugin -> get_name_field_values($value,$field);
        $display_value = [$name_field_values];
      }

      if($field -> type =='consent') {
        $consent_field_values = self::$plugin -> get_consent_field_values($value,$field);
        $display_value = [$consent_field_values];
      }

      if($field -> type =='address') {
        $address_field_values = self::$plugin -> get_address_field_values($value,$field);
        $display_value = [$address_field_values];
      }

      /**
       * POST fields
       */

      if($field -> type =='post_image') {
        $post_image_field_values = self::$plugin -> get_post_image_field_values($value,$field);
        $display_value = [$post_image_field_values];
      }

      if($field -> type =='post_category' || $field -> type =='post_tags') {
        $post_taxonomy_field_values = self::$plugin -> get_post_taxonomy_field_values($value,$field);
        $display_value = $post_taxonomy_field_values;
      }

      // do a trick here - since 'post_custom_field' actually use other field types as an Input type just switch values  here
      if($field -> type =='post_custom_field' ) {
        $field -> type = $field -> inputType;
      }

      /**
       * PRICING fields
       */

      if($field -> type =='shipping') {
        $shipping_field_values = self::$plugin -> get_shipping_field_values($value,$field);
        $display_value = [$shipping_field_values];
      }

      if($field -> type =='product') {
        $product_field_values = self::$plugin -> get_product_field_values($value,$field);
        $display_value = [$product_field_values];
      }

      if($field -> type =='option') {
        $option_field_values = self::$plugin -> get_option_field_values($value,$field);
        $display_value = $option_field_values;
      }

      /**
       * ADDONS fields
       */

      //Survey addon
      if($field -> type =='survey') {

        $display_value = [];
        $survey_field_values = '';

        if($field -> inputType == 'likert'){
          $survey_field_values = self::$plugin -> get_survey_field_values($value,$field, 'likert');
          $display_value = $survey_field_values;
        }

        if($field -> inputType == 'rank'){
          $survey_field_values = self::$plugin -> get_survey_field_values($value,$field, 'rank');
          $display_value = $survey_field_values;
        }

        if($field -> inputType == 'rating') {
          $survey_field_values = \RGFormsModel::get_choice_text( $field, $value );
          $display_value = $survey_field_values;
        }

        if($field -> inputType == 'checkbox' || $field -> inputType =='radio'|| $field -> inputType =='select') {
          $display_value =  self::$plugin -> get_choice_type_field_values( $value ,$field);
        }

        if($field -> inputType == 'text' || $field -> inputType == 'textarea') {
          $display_value = $value;
        }
      }

      // Quiz addon
      if($field -> type == 'quiz' || $field -> type == 'poll'){
        if($field -> inputType == 'checkbox' || $field -> inputType =='radio'|| $field -> inputType =='select') {
          $display_value =  self::$plugin -> get_choice_type_field_values( $value ,$field);
        }
      }

      // add filter to force gform as entry detail(the view in entries admin area)
      add_filter('gform_is_entry_detail', function($is_entry_detail) use($field,$form) {

        /**
         * for survey we need forced 'false'
         * otherwise Fatal error: Uncaught Error: Class 'GFEntryDetail' not found in ... gravityformssurvey\includes\class-gf-field-likert.php on line 465
         * bug?
         */
        if($field -> type == 'poll') return true;
        return false;
      });

      add_filter('gform_entry_detail_grid_display_empty_fields', function($is_entry_detail){
        return true;
      });

      //apply_filters( 'gform_entry_detail_grid_display_empty_fields', $display_empty_fields, $form, $lead );

       // field types for which we return a list (sub)loop
      $list_type_field_types =[
        'multiselect',
        'checkbox',
        'radio',
        'select',
        'name',
        'product',
        'shipping',
        'address',
        'consent',
        'fileupload',
        'post_image',
        'post_category',
        'post_tags',
        'option',
      ];

      // user object
      //$user_id =( isset($this->args['user']) && !empty($this->args['user']) ) ? ( $this->args['user'] === 'current'  ? get_current_user_id() : $this->args['user'] ) : get_current_user_id();


      switch ($field_name) {

        // data
        case 'field_id': return $field_id;
        case 'field_type': return $field -> type;
        case 'field_input_type': return $field -> inputType;
        case 'field_is_addon': return $field -> type == 'survey' || $field -> type == 'quiz' || $field -> type == 'poll';
        case 'field_is_loop': return in_array( $field -> type, $list_type_field_types );
        case 'field_label':return $label;
        case 'field_value_html':
          if($field -> type =='post_image'){
            return $field -> get_value_entry_detail( $value );

          }elseif($field -> type =='post_category'){
            \GFCommon::add_categories_as_choices( $field, $value );

          }elseif($field -> type =='quiz'){
            if (self::$plugin -> is_addon_active('GFQuiz')) {
              $GFQuiz = new \GFQuiz;
              return '<form class="tgbl-gf-'.$field -> type.'-wrap">'.$GFQuiz -> display_quiz_on_entry_detail( $value, $field, $entry, $form ).'</form>';
            } else{
              return '';
            }

          }elseif($field -> type =='poll'){
            if (self::$plugin -> is_addon_active('GFPolls')) {
              $GFPolls = new \GFPolls;
              return '<form class="tgbl-gf-'.$field -> type.'-wrap">'.$GFPolls -> display_poll_on_entry_detail( $value, $field, $entry, $form ).'</form>';
            } else{
              return '';
            }
          }
          return '<form class="tgbl-gf-'.$field -> type.'-wrap">'.$field -> get_field_input( $form, $value ).'</form>';

        case 'field_value':
          if( $field -> type == 'survey' || $field -> type == 'quiz' || $field -> type == 'poll') {
            if ( $field -> inputType == 'likert' || $field -> inputType == 'rank' || $field -> inputType == 'radio' || $field -> inputType == 'checkbox' || $field -> inputType == 'select' ) {
              return self ::$loop -> create_type( 'list', $display_value );
            }
            return $display_value;

          } else{
            if ( in_array( $field -> type, $list_type_field_types ) ) {
              return self ::$loop -> create_type( 'list', $display_value );
            }
            return $display_value;
          }

          default:
          //remove prefix for parent fields
          $field_name = self::$plugin -> field_prefix_handle( $field_name, $prefix, true );
          return parent::get_item_field( $field, $field_name, $args );
      }
    }
  };

  EntryFieldsLoop::$loop = $loop;
  EntryFieldsLoop::$plugin = $plugin;

  EntryFieldsLoop::$config['query_args'] =  [
    'entry_id'  =>
      [
        'description' => 'Entry ID the field belongs to".',
        'type'        => 'string',
      ],
    'field_id'      => [
      'description' => 'Field ID(s) - returns specified fields for the entry',
      'type'        => ['string', 'array'],
    ],
  ];

  EntryFieldsLoop::$config['fields'] = [
  'id'=> [ 'description' => 'Entry Field ID' ],
  'label'=> [ 'description' => 'Entry Field Label' ],
  'value'=> [ 'description' => 'Entry Field Value (for some field types it is a loop with keys)' ],
  'value_html'=> [ 'description' => 'Entry Field Value - HTML presentation' ],
  'type'=> [ 'description' => 'Entry Field Type' ],
  'input_type'=> [ 'description' => 'Entry Field Input Type - some field types have various input types' ],
  'is_addon'=> [ 'description' => 'Is Entry Field of GF AddOn'],
  'is_loop'=> [ 'description' => 'Is Entry Field Value a loop' ],
  ];

  $loop->register_type( EntryFieldsLoop::class );
