<h1>Entry Field</h1>

<?php

  $framework = tangible();
  $tester = tangible_tester();
  $loop = tangible_loop();
  $logic = tangible_logic();
  $html = tangible_template();

  $plugin = tangible_loops_for_gravity_forms();

  // $tester->enqueue(); // JavaScript tests

  $test = $tester->start();

  // Config

  $test('Entry Field loop config', function($it) use ($loop,$plugin) {

    $config = $loop->get_type_config('gravity_form_entry_fields', false);

    // Inspect config
    // tangible()->see('config', $config);

    $it('exists as loop type gravity_form_entry_fields', !empty($config));

    if ($config===false) return; // Skip the rest

    $required_properties = [
      'name',
      'title',
      'category',
      'description',
      'query_args',
      'fields'
    ];

    foreach ($required_properties as $key) {
      $it("has property \"$key\"", isset($config[ $key ]));
    }
  });


  // Loop type

  $test('GF Entry Field loop type', function($it) use ($loop,$plugin) {

    $entries = $plugin->get_entries([]);
    $entry_fields_loop = $loop('gravity_form_entry_fields',['entry_id' => array_shift($entries)]);

    // Inspect loop instance
    //tangible()->see('entry_fields_loop', $entry_fields_loop);

    $it('can create loop type gravity_form_entry_fields', !empty($entry_fields_loop));

    if (empty($entry_fields_loop)) return; // Skip the rest

    // Items

    $entry_fields = $entry_fields_loop->get_items();
    $it('returns entry_fields from method get_items() ', !empty($entry_fields));
  });


  // Loop item

  $test('GF Entry Field loop item', function($it) use ($loop,$plugin) {

    $entries = $plugin->get_entries([]);
    $entry_fields_loop = $loop('gravity_form_entry_fields',['entry_id' => array_shift($entries)]);

    $it('loop has next item', $entry_fields_loop->has_next());

    if ( ! $entry_fields_loop->has_next() ) return; // Skip the rest

    $entry_fields_count = 0;

    while($entry_fields_loop->has_next()) {
      $entry_fields_loop->next();
      $entry_fields_count++;
    }

    $it('loops every item', $entry_fields_count===$entry_fields_loop->get_items_count());

    $entry_fields_loop->reset();

    $it('loop can be reset', $entry_fields_loop->has_next());

    $entry_fields_loop->next();
    $entry_field = $entry_fields_loop->get_current();

    $it('has current item', !empty($entry_field));
  });

// Fields

  $test('GF Entry Field fields', function($it) use ($plugin, $loop) {

    $entries = $plugin->get_entries([]);
    $entry_id = array_shift($entries);
    $entry_fields_loop = $loop('gravity_form_entry_fields',['entry_id' => $entry_id]);
    $entry_field_id = $entry_fields_loop->next();

    //tangible()->see('course', $entry_fields_loop);

    //field_id
    $check = $entry_fields_loop->get_field('id');
    $value=  $entry_field_id; // Correct value
    $it('id', $value===$check);

    //field_label
    $entry = \GFAPI::get_entry( $entry_id );
    $form_id = $entry['form_id'];
    $form = \GFAPI::get_form( $form_id );

    foreach ( $form['fields'] as $f ) {
      if ( $f -> id == $entry_field_id ) {
        $field = $f;
        break;
      }
    }
    $label = esc_html( \GFCommon::get_label( $field ) );
    $check = $entry_fields_loop->get_field('label');

    $value = $label; // Correct value
    $it('label', $value==$check);

    //field_type
    $type=$field -> type;
    $check = $entry_fields_loop->get_field('type');

    $value = $type; // Correct value
    $it('type', $value==$check);

    //field_is_addon
    $is_addon = $field -> type == 'survey' || $field -> type == 'quiz' || $field -> type == 'poll';
    $check = $entry_fields_loop->get_field('is_addon');

    $value = $is_addon; // Correct value
    $it('is_addon', $value==$check);
  });

  // Logic

  /*$test('GF Entry Field logic', function($it) use ($plugin, $loop, $html) {

    $entry_fields_loop = $loop('gravity_form_entry_fields');
    $course = $entry_fields_loop->next();

    $loop->push_context( $entry_fields_loop );

    $check = $html->if([
      'field' => 'course',
      'compare' => 'is',
      'value' => 'completed',
    ]);

    $user_id  = get_current_user_id();
    $student = llms_get_student( $user_id );

    if( empty($student) ){
      $it('please login', false);
    } else {
      $so = new \LLMS_Student( $student -> get( 'id' ) );
      $completed_ids = $so -> get_completed_entry_fields();
      $correct = in_array( $course -> ID, $completed_ids ) ? true : false;

      $loop->pop_context();

      $it( 'course completed', $correct === $check );
      if ( $correct !== $check )
        tgbl() -> see( 'expected', $correct, 'given', $check );
    }
  });*/


// Report

$test->report();
