<?php
  namespace Tangible\Loop\Integrations\GravityForms;

  use Tangible\Loop\BaseLoop;


  class EntryLoop extends BaseLoop {

    static $loop; // See after class
    static $plugin;// See after class

    static $config = [
      'name'       => 'gravity_form_entry', // Must correspond with $loop( $loop_type, .. )
      'title'      => 'GravityForm Entry',
      'category'    => 'gravityforms',
      'description' => 'Loop through GravityForms entries',
    ];

    function __construct( $args = [] ) {
      parent::__construct( $args );
    }

    function create_query_args( $args = [] ) {
      $query_args = parent::create_query_args( $args );
      return $query_args;
    }


    function run_query( $query_args ) {
      $args = parent::run_query( $query_args);
      return self::$plugin->get_entries($query_args);
    }

    function get_items_from_query( $query ) {

      $plugin = self::$plugin;
      $items = parent::get_items_from_query( $query );

      // Filter items
      // entry ids
      if ( ! empty($this->args['entry_id']) ) {

        $match_types =  $plugin -> string_to_array($this->args['entry_id']);

        if( !empty($match_types) ) {
          $items = array_values( array_filter( $items, function ( $item ) use ( $match_types,$plugin ) {
            return in_array($item,$match_types);
          } ) );
        }
      }

      //tgbl()->see($items);
      return $this->items = $items;
    }

    function get_item_field( $entry_id, $field_name, $args = [] ) {

      $prefix = 'entry';
      // add prefix to fields, backward compatibility
      $field_name = self::$plugin -> field_prefix_handle( $field_name, $prefix);

      if ( !$entry_id ) return;
      $entry = \GFAPI::get_entry( $entry_id );


      $form_id = $entry['form_id'];
      $form = \GFAPI::get_form( $form_id );

      //tgbl()->see($entry);

      // user object
      $user_id =( isset($this->args['user']) && !empty($this->args['user']) ) ? ( $this->args['user'] === 'current'  ? get_current_user_id() : $this->args['user'] ) : get_current_user_id();


      switch ($field_name) {

        // data
        case 'entry_id': return $entry_id;
        case 'entry_form_name': return $form['title'];
        case 'entry_form_id': return $form_id;
        case 'entry_date_created':
        case 'entry_date_updated':
          $d = $field_name =='entry_date_created' ? $entry['date_created'] : $entry['date_updated'];
          $date_format = isset($args['date_format'])  ? $args['date_format'] : get_option('date_format');
          $date = tangible_date()->createFromTimeStamp(strtotime($d));
          return $date->format( $date_format );

        case 'entry_is_starred': return $entry['is_starred'];
        case 'entry_is_read': return $entry['is_read'];
        case 'entry_currency': return $entry['currency'];
        case 'entry_payment_status': return !empty($entry['payment_status']) ? $entry['payment_status'] : '';
        case 'entry_payment_date': return !empty($entry['payment_date']) ? $entry['payment_date'] : '';
        case 'entry_payment_amount': return !empty($entry['payment_amount']) ? $entry['payment_amount'] : '';
        case 'entry_payment_method': return !empty($entry['payment_method']) ? $entry['payment_method'] : '';
        case 'entry_transaction_id': return !empty($entry['transaction_id']) ? $entry['transaction_id'] : '';
        case 'entry_is_fulfilled': return !empty($entry['is_fulfilled']) ? $entry['is_fulfilled'] : '';
        case 'entry_created_by': return !empty($entry['created_by']) ? $entry['created_by'] : '';
        case 'entry_transaction_type': return !empty($entry['transaction_type']) ? $entry['transaction_type'] : '';
        case 'entry_status': return !empty($entry['status']) ? $entry['status'] : '';

        case 'entry_fields':
          $fields = self::$loop->create_type('gravity_form_entry_fields', [ 'entry_id' => $entry_id  ]);
          return $fields;

        case 'entry_grid':
          require_once( \GFCommon::get_base_path() . '/entry_detail.php' );
          ob_start();
          \GFEntryDetail::lead_detail_grid( $form, $entry );
          $grid = ob_get_clean();
          return $grid;

        /// Survey add on survey_score

        case 'entry_survey_score':
          if(self::$plugin -> is_addon_active('GFSurvey')) {
            $GFSurvey = new \GFSurvey();
            return $GFSurvey -> get_survey_score( $form, $entry );
          } else{
            return '';
          }

         /// Quiz add on - quiz score,

        case 'entry_quiz_score':
        case 'entry_quiz_percent':
        case 'entry_quiz_grade':
        case 'entry_quiz_is_pass':
          if(self::$plugin -> is_addon_active('GFQuiz')) {
            $GFQuiz = new \GFQuiz;
            $quiz_results = $GFQuiz -> get_quiz_results( $form, $entry );
            $v = str_replace('entry_quiz_','', $field_name);
            return $quiz_results[$v];
          } else{
            return '';
          }

        case 'entry_results':

          //return $GFSurvey -> results_fields( $form ) ;

        default:
          //remove prefix for parent fields
          $field_name = self::$plugin -> field_prefix_handle( $field_name, $prefix, true );
          return parent::get_item_field( $entry, $field_name, $args );
      }
    }
  };

  EntryLoop::$loop = $loop;
  EntryLoop::$plugin = $plugin;
  EntryLoop::$config['query_args'] =  [
    'form_id'      => [
      'description' => 'Form ID(s)',
      'type'        => ['string', 'array'],
    ],
    'entry_id'      => [
      'description' => 'Specific Entry ID(s)',
      'type'        => ['string', 'array'],
    ],
    'user'      => [
      'description' => 'Entry created by: User ID or "current".',
      'type'        => 'string',
    ],
    'start_date'    => [
      'description' => 'Start date - the earliest date from which we retrieve entries(if omitted from beginning). Accepts formats like "-10 days", "-3 hours", "yesterday", "April 22, 2021, 3:00 pm", "2021-04-22 15:01:46" etc.',
      'type'        => 'string',
    ],
    'end_date'    => [
      'description' => 'End date - the latest date to which we retrieve entries(if omitted until now). Accepts formats like "-10 days", "-3 hours", "yesterday", "April 22, 2021, 3:00 pm", "2021-04-22 15:01:46" etc.',
      'type'        => 'string',
    ],
    'orderby'    => [
      'description' => 'Order by one of: "id", "form_id", "date_created", "is_starred", "currency",	"payment_status","payment_date", "payment_amount",	"payment_method", "transaction_id", "is_fulfilled", "created_by", "transaction_type", "status"',
      'default' => 'id',
      'type'        => 'string',
    ],
    'order'    => [
      'description' => 'Order: "asc" (ascending) or "desc" (descending)',
      'default' => 'asc',
      'type'        => 'string',
    ],
    'is_starred'    => [
      'description' => 'Is entry starred "0/1"',
      'type'        => 'string',
    ],
    'is_read'    => [
      'description' => 'Is entry read "0/1"',
      'type'        => 'string',
    ],
    'offset'    => [
      'description' => 'Offset, argument for use with page size',
      'type'        => 'string',
    ],
    'page_size'    => [
      'description' => 'Entries page size',
      'default'        => '20',
      'type'        => 'string',
    ],
  ];

  EntryLoop::$config['fields'] = [
    'id'=> [ 'description' => 'Entry ID' ],
    'form_name'=> [ 'description' => 'Entry\'s Form name' ],
    'form_id'=> [ 'description' => 'Entry\s Form ID' ],
    'fields'=> [ 'description' => 'Entry Fields' ],
    'grid'=> [ 'description' => 'Entry detail view(like in admin)' ],

    'entry_date_created'=> [ 'description' => 'Entry date created' ],
    'entry_date_updated'=> [ 'description' => 'Entry date updated' ],
    'entry_is_starred'=> [ 'description' => 'Entry is starred or not' ],
    'entry_is_read'=> [ 'description' => 'Entry is read or not' ],
    'entry_currency'=> [ 'description' => 'Entry currency' ],
    'entry_payment_status'=> [ 'description' => 'Entry payment status'],
    'entry_payment_date'=> [ 'description' => 'Entry payment date' ],
    'entry_payment_amount'=> [ 'description' => 'Entry payment amount' ],
    'entry_payment_method'=> [ 'description' => 'Entry payment method' ],
    'entry_transaction_id'=> [ 'description' => 'Entry transaction id' ],
    'entry_is_fulfilled'=> [ 'description' => 'Entry is fulfilled or not' ],
    'entry_created_by'=> [ 'description' => 'Entry created by User id' ],
    'entry_transaction_type'=> [ 'description' => 'Entry transaction type' ],

    'entry_survey_score'=> [ 'description' => 'Survey AddOn - survey score' ],

    'entry_quiz_score'=> [ 'description' => 'Quiz AddOn - quiz score' ],
    'entry_quiz_percent'=> [ 'description' => 'Quiz AddOn - quiz percent ' ],
    'entry_quiz_grade'=> [ 'description' => 'Quiz AddOn - quiz grade' ],
    'entry_quiz_is_pass'=> [ 'description' => 'Quiz AddOn - if quiz is pass' ],
  ];

  $loop->register_type( EntryLoop::class );