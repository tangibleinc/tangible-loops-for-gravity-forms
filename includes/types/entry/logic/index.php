<?php

/**
 * Register with Logic module
 */

$logic->extend_rules_by_category(
  'gravity_form_entry',
  require_once __DIR__.'/rules.php',
  require_once __DIR__.'/evaluate.php'
);
