<?php

/**
 * Evaluate
 *
 * @see tangible-loops-and-logic/vendor/tangible/template/logic/evaluate.php
 */
return $plugin->evaluate_entry_logic_rule = function($rule) use ($plugin, $loop, $logic, $html) {

  $condition = true;

  $field = @$rule['field'];
  $compare = @$rule['operand'];
  $value = @$rule['value'];

  $current_loop = $loop->get_current('gravity_form_entry');
  if(empty($current_loop)) return false;

  $entry_id = $current_loop->get_field('id');
  //$entry_id = 49;

  $entry = \GFAPI::get_entry( $entry_id );

  switch ($field) {
    case 'created_by_user':
      switch ($compare) {
        case 'is':
        case 'is_not':
          if( $value == 'current' ) {
            $value = get_current_user_id();
          }
          $created_by = $entry['created_by'];
          $condition = $value == $created_by;
          if ($compare==='is_not') $condition = !$condition;
          break;
      }
      break;
    case 'is_starred':
      switch ($value) {
        case 'true':
          $condition = !empty($entry['is_starred']);
          if ($compare==='is_not') $condition = !$condition;
          break;
      }
      break;

      case 'is_read':
      switch ($value) {
        case 'true':
          $condition = !empty($entry['is_read']);
          if ($compare==='is_not') $condition = !$condition;
          break;
      }
      break;
  }

  return $condition;
};

