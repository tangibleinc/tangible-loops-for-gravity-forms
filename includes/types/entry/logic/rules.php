<?php
  /**
   * Rule definitions
   *
   */
  return $plugin->entry_logic_rules = [
    // created by
    [ 'name' => 'created_by_user',
      'label' => 'GF Entry created by(User id)',
      'operands' => [
        [ 'name' => 'is', 'label' => 'is' ],
        [ 'name' => 'is_not', 'label' => 'is not' ],
      ],
      'values' => [
        'type' => 'text',
      ],
    ],
    // is_starred
    [ 'name' => 'is_starred',
      'label' => 'GF Entry starred',
      'operands' => [
        [ 'name' => 'is', 'label' => 'is' ],
        [ 'name' => 'is_not', 'label' => 'is not' ],
      ],
      'values' => [
        'type' => 'select',
        'options' => [
          [ 'name' => 'true', 'label' => 'true' ]
        ],
      ],
    ],

    // is_starred
    [ 'name' => 'is_read',
      'label' => 'GF Entry read',
      'operands' => [
        [ 'name' => 'is', 'label' => 'is' ],
        [ 'name' => 'is_not', 'label' => 'is not' ],
      ],
      'values' => [
        'type' => 'select',
        'options' => [
          [ 'name' => 'true', 'label' => 'true' ]
        ],
      ],
    ],
  ];

