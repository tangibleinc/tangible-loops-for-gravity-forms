<h1>Course</h1>

<?php

  $framework = tangible();
  $tester = tangible_tester();
  $loop = tangible_loop();
  $logic = tangible_logic();
  $html = tangible_template();

  $plugin = tangible_loops_for_gravity_forms();

  // $tester->enqueue(); // JavaScript tests

  $test = $tester->start();

  // Config

  $test('Entry loop config', function($it) use ($loop) {

    $config = $loop->get_type_config('gravity_form_entry', false);

    // Inspect config
    // tangible()->see('config', $config);

    $it('exists as loop type gravity_form_entry', !empty($config));

    if ($config===false) return; // Skip the rest

    $required_properties = [
      'name',
      'title',
      'category',
      'description',
      'query_args',
      'fields'
    ];

    foreach ($required_properties as $key) {
      $it("has property \"$key\"", isset($config[ $key ]));
    }
  });


  // Loop type

  $test('GF Entry loop type', function($it) use ($loop) {

    $entry_loop = $loop('gravity_form_entry');

    // Inspect loop instance
    // tangible()->see('entry_loop', $entry_loop);

    $it('can create loop type gravity_form_entry', !empty($entry_loop));

    if (empty($entry_loop)) return; // Skip the rest

    // Items

    $entrys = $entry_loop->get_items();

    $it('returns entrys from method get_items() ', !empty($entrys));

  });


  // Loop item

  $test('GF Entry loop item', function($it) use ($loop) {

    $entry_loop = $loop('gravity_form_entry');

    $it('loop has next item', $entry_loop->has_next());

    if ( ! $entry_loop->has_next() ) return; // Skip the rest

    $entry_count = 0;

    while($entry_loop->has_next()) {
      $entry_loop->next();
      $entry_count++;
    }

    $it('loops every item', $entry_count===$entry_loop->get_items_count());

    $entry_loop->reset();

    $it('loop can be reset', $entry_loop->has_next());

    $entry_loop->next();
    $entry = $entry_loop->get_current();

    $it('has current item', !empty($entry));
  });

// Fields

  $test('GF Entry fields', function($it) use ($plugin, $loop) {

    $entry_loop = $loop('gravity_form_entry');
    $entry_id = $entry_loop->next();

    //entry_id
    $check = $entry_loop->get_field('id');
    $value=  $entry_id; // Correct value
    $it('ID', $value===$check);

    //entry_form_name
    $entry = \GFAPI::get_entry( $entry_id );
    $form_id = $entry['form_id'];
    $form = \GFAPI::get_form( $form_id );
    $check = $entry_loop->get_field('form_name');
    $value =$form['title'];
    $it('form_name', $value===$check);

    //entry_form_id
    $check = $entry_loop->get_field('form_id');
    $value =$form_id;
    $it('form_id', $value===$check);
    ///tangible()->see('$form', $form);
  });

  // Logic

  $test('GF Entry logic', function($it) use ($plugin, $loop, $html) {

    $entry_loop = $loop('gravity_form_entry',['user' => 'current']);
    $entry_id = $entry_loop->next();

    $loop->push_context( $entry_loop );

    $entry = \GFAPI::get_entry( $entry_id );
    $created_by = $entry['created_by'];

    $check = $html->if([
      'field' => 'entry_created_by',
      'compare' => 'is',
      'value' => $created_by,
    ]);

    $user_id  = get_current_user_id();

    if( empty( $user_id ) ){
      $it('please login', false);
    } else {

      $correct = $created_by ==  $user_id ;
      $loop->pop_context();

      $it( 'entry_created_by', $correct === $check );
      if ( $correct !== $check )
        tgbl() -> see( 'expected', $correct, 'given', $check );
    }
  });

  // Subloops  access_type

  $test('Entry subloops', function($it) use ($plugin, $loop, $html) {

    $entry_loop = $loop('gravity_form_entry');
    $entry_id = $entry_loop->next();

    $entry_fields_loop = $loop('gravity_form_entry_fields', [ 'entry_id' => $entry_id  ]);

    $correct_number = count($plugin -> get_entry_fields(['entry_id' => $entry_id]));
    $entry_fields_number = $entry_fields_loop -> get_total_items_count();

    $correct_entry_fields = $plugin -> get_entry_fields(['entry_id' => $entry_id]);
    $entry_fields = $entry_fields_loop -> get_total_items();

    $it_is_correct = true;
    foreach($correct_entry_fields as $cl ){
     if( !in_array( $cl, $entry_fields) ){
       $it_is_correct = false;
       break;
     }
    }
    $it('entry fields subloop returns a Loop', $loop->is_instance($entry_fields_loop));
    $it('entry fields subloop returns number of items correctly', $correct_number==$entry_fields_number);
    $it('entry lfields subloop returns correct lessons', $it_is_correct );
   });

   // Query filtering

  $test('Entry query args', function($it) use ($plugin, $loop, $html) {

    $forms= \GFAPI::get_forms();
    if(!$forms) return;

    $form_id=0;
    foreach($forms as $f){
      $count_entries = \GFAPI::count_entries( $f['id']);
      if($count_entries){
        $form_id = $f['id'];
      }
    }

    $entry_loop = $loop('gravity_form_entry',['form_id'=>$form_id]);
    $entry = $entry_loop->next();
    $entrys = $entry_loop->get_items();

    $it_is_correct = true;
    foreach($entrys as $c ){
     $en = \GFAPI::get_entry( $c );
     $f_id = $en['form_id'];
     if( $f_id != $form_id ){
       $it_is_correct = false;
       break;
     }
    }
    $it('Filtering by \'form_id\' query argument returns corresponding entrys', $it_is_correct);
  });


// Report

$test->report();
