  <?php
  defined('ABSPATH') or die();


  $plugin->get_entries = function($args=[]) use ($plugin) {
    $defaults =
      [
      'ids'=>true,
      'form_id'=>0, // 0 for all forms, id or array of ids
      'search_criteria'=>['status' => 'active'],
      'offset' => 0,
      'page_size' => 20,
      'total_count'=>null,
      // args for search criteria
      'user' => 0,
      'start_date' => null,
      'end_date' => null,
      'order' => 'asc',
      'orderby' => 'id',
      'is_starred' => null,
      'is_read' => null,
      ];


    $entry_args = wp_parse_args($args,$defaults);

    $form_id = $entry_args['form_id'];
    if(!empty($form_id)) {
      $form_id = $plugin -> string_to_array( $entry_args['form_id'] );
    }

    $search_criteria = $entry_args['search_criteria'];

    //filter by user
    if(!empty($entry_args[ 'user'])){
      $user_id = ( $entry_args[ 'user'] == 'current')  ? get_current_user_id() : $entry_args[ 'user'];
      $search_criteria['field_filters'][] = array( 'key' => 'created_by', 'value' => $user_id );
    }

    //start date
    if(!empty($entry_args[ 'start_date'])) {
      $start_date = date( 'Y-m-d  H:i:s', strtotime($entry_args[ 'start_date']) );
      $search_criteria[ 'start_date' ] = $start_date;
    }

    //end date
    if(!empty($entry_args[ 'end_date'])) {
      $end_date = date( 'Y-m-d  H:i:s', strtotime($entry_args[ 'end_date']) );
      $search_criteria[ 'end_date' ] = $end_date;
    }

    // is_starred
    if(!empty($entry_args[ 'is_starred'])) {
      $search_criteria['field_filters'][] = array( 'key' => 'is_starred', 'value' => $entry_args[ 'is_starred'] );
    }

    // is_read
    if(!empty($entry_args[ 'is_read'])) {
      $search_criteria['field_filters'][] = array( 'key' => 'is_read', 'value' => $entry_args[ 'is_read'] );
    }

    // sorting
    $sort_field = $entry_args[ 'orderby'];
    $direction = $entry_args[ 'order'];
    $sorting = array( 'key' => $sort_field, 'direction' => $direction );


    //paging
    $paging = ['offset' => $entry_args['offset'], 'page_size' => $entry_args['page_size']];


    $entries = \GFAPI::get_entries( $form_id, $search_criteria, $sorting, $paging , $entry_args[ 'total_count'] );

    // if 'ids' flag set to false, return entry objects array
    if( empty($entry_args['ids'])){
      return $entries;
    }

    $entry_ids=[];
    foreach($entries as $e){
      $entry_ids[]=$e['id'];
    }

    return $entry_ids;
  };
