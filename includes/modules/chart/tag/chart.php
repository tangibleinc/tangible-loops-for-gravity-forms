<?php

  $html->add_open_tag('GFChartData', function($atts, $nodes) use ($plugin,$html,$framework,$loop) {

    // Default options

    $options =
      [
        'labels'   => [],
        'datasets' =>
          [
            'label' => '',
            'data'  => [],
            'backgroundColor' => [],
            'borderColor' => [],
          ],

      ];

    foreach ( $atts as $key => $value ) {
      $js_key = $framework -> camel_case( $key );
      if ( !isset( $options[ $js_key ] ) ) continue;

      // Cast to same type as default

      $options[ $js_key ] = is_bool( $options[ $js_key ] ) ? $atts[ $key ] === 'true' : ( is_int( $options[ $js_key ] ) ? (int) $atts[ $key ] : ( $atts[ $key ] ) );
    }

    $tag_atts =
      [

        'class' => 'tangible-chart-data' . ( isset( $atts[ 'class' ] ) ? ' ' . $atts[ 'class' ] : '' ),

        /**
         * Support for page builders with dynamic HTML fetch & insert
         *
         * @see /module-loader in Template module
         */
        'data-tangible-dynamic-module' => 'chart-data'
      ];

    if ( isset( $atts[ 'id' ] ) )  $tag_atts[ 'id' ] = $atts[ 'id' ];

    // Check for mandatory arguments

    if ( !isset( $atts[ 'source' ] ) || empty( $atts[ 'source' ] ))  return;
    if ( !isset( $atts[ 'form_id' ] ) || empty( $atts[ 'form_id' ] ) )  return;
    if ( !isset( $atts[ 'field_id' ] ) || empty( $atts[ 'field_id' ] ) )  return;

    // Check if Add-On active

    $source= $atts[ 'source' ];
    $add_on_condition = $plugin -> add_on_condition($source);
    if(!$add_on_condition) return;

    // Form

    $form_id = $atts[ 'form_id' ];
    $form = \GFAPI ::get_form( $form_id );
    if(empty($form)) return;

    // Get field objects array

    $field_ids = $plugin -> string_to_array( $atts[ 'field_id' ] );
    $field_objects_array = $plugin -> get_filtered_fields( $field_ids, $form, true);
    if(empty($field_objects_array)) return;

    //tgbl() -> see($field_objects_array); die;

    // Filter results by entry ids if required by argument

    $search_criteria = [];

    $entry_ids = ( isset( $atts[ 'entry_id' ] ) || !empty( $atts[ 'entry_id' ] )) ? $plugin -> string_to_array( $atts[ 'entry_id' ] ) : [];
    if(is_array($entry_ids) && !empty($entry_ids)) $search_criteria['field_filters'][] = array( 'key' => 'id', 'operator' => 'in', 'value' => $entry_ids );

    // Filter results by User ID if required by argument

    $user_ids = ( isset( $atts[ 'user_id' ] ) || !empty( $atts[ 'user_id' ] )) ? ( $atts[ 'user_id' ] == "current" ? [get_current_user_id()] : $plugin -> string_to_array( $atts[ 'user_id' ] )) : [];
    if(is_array($user_ids) && !empty($user_ids)) $search_criteria['field_filters'][] = array( 'key' => 'created_by', 'operator' => 'in', 'value' => $user_ids );

    //TODO add more search criteria ?

    // Get Survey results data

    $results_data =  $plugin -> get_results_data($form, $field_objects_array, $search_criteria, $source );
    if(empty($results_data) || !isset($results_data['field_data'])) return;


    // Get first field in array - all fields are the same type

    $field = $field_objects_array[0];
    $field_type = $field -> type;
    $field_input_type = $field -> inputType;
    $field_inputs = $field -> inputs;
    $field_likert_multirow = $field -> gsurveyLikertEnableMultipleRows;

    // Get labels from the fields

    $multi_labels = isset($atts[ 'multi_labels' ]) && $atts[ 'multi_labels' ] == 'true';

    $score_frequencies = isset($atts[ 'score_frequencies' ]) && $atts[ 'score_frequencies' ] == 'true';
    $grade_frequencies = isset($atts[ 'grade_frequencies' ]) && $atts[ 'grade_frequencies' ] == 'true';

    $is_display_frequency = false;
    $frequency = '';
    foreach(['score_frequencies','grade_frequencies'] as $frq ){
      if ($$frq) {
        $is_display_frequency = $$frq;
        $frequency = $frq;
      }
    }

    if($field_type == 'quiz' && $is_display_frequency){
      $labels = $plugin -> get_quiz_score_grade_data( $results_data, $form, $field_objects_array, $frequency, 'labels' );
    }else {
      $labels = $plugin -> get_labels( $field_objects_array, $multi_labels );
    }
    if(empty($labels)) return;

    // Prepare data for chart datasets

    if($field_type == 'survey' || $field_type == 'quiz' || $field_type == 'poll' ){

      if($field_type == 'quiz' && $is_display_frequency ) {

        $chart_datasets[] = $plugin -> get_quiz_score_grade_data( $results_data, $form, $field_objects_array, $frequency, 'data' );

      }else{

        if($field_input_type == 'likert' ){
          $chart_datasets = $plugin -> survey_likert_datasets( $form, $results_data['field_data'], $field_inputs, $field_likert_multirow );

        } elseif($field_input_type == 'rank' || $field_input_type == 'rating' || $field_input_type == 'radio' || $field_input_type == 'checkbox' || $field_input_type == 'select'){
          $chart_datasets = $plugin -> survey_datasets( $form, $results_data['field_data'] );
        }
      }
    }

    //tgbl() -> see($chart_datasets); die;
    $options['labels'] = $labels;
    $options['datasets'] = $chart_datasets;

    $tag_atts['data-tangible-dynamic-module-options'] = json_encode($options);

    return $html->render_tag('div', $tag_atts, $nodes);
  });