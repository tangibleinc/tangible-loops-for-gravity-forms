  <?php
  defined('ABSPATH') or die();


  /**
   * Get chart data
   *
   * @param $source  - 'survey', 'poll', 'quiz'
   *
   * @return bool $condition
   */
  $plugin -> add_on_condition = function($source = '') use ($plugin) {

    $condition = true;

    if(empty($source) || !in_array( $source, [ 'survey', 'quiz',  'poll' ] ) ) return false;

    $source = $source =='survey' ? 'GFSurvey' : ( $source =='quiz' ? 'GFQuiz' : ( $source =='poll' ? 'GFPolls' : '' ));
    $add_on_active = $plugin -> is_addon_active($source);
    if ( $source  && !$add_on_active ) return false;

    return $condition;
  };


  /**
   * Get survey results data
   *
   * @param object $form
   * @param array $fields
   * @param array $search_criteria
   * @param string $source  - 'survey', 'poll', 'quiz'
   *
   * @return array $results_data for a source
   */
  $plugin -> get_results_data = function($form = null, $fields=null, $search_criteria = [], $source = '' ) use ($plugin) {

    if (empty($form) || empty($fields)) return [];
    if(empty($source) || !in_array( $source, [ 'survey', 'quiz',  'poll' ] ) ) return [];

    $add_on = $source =='survey' ? new \GFSurvey() : ( $source =='quiz' ? new \GFQuiz() : ( $source =='poll' ? new \GFPolls() : null ));
    if ( empty($add_on) ) return [];

    $add_on_slug = $source =='survey' ? 'gravityformssurvey' : ( $source =='quiz' ? 'gravityformsquiz' : ( $source =='poll' ? 'gravityformspolls' : null ));

    if ( ! class_exists( 'GFResults' ) ) {
      require_once( \GFCommon ::get_base_path() . '/includes/addon/class-gf-results.php' );
    }

    $GFResults = new \GFResults($add_on_slug, $add_on->get_results_page_config());
    $results_data = $GFResults -> get_results_data( $form, $fields, $search_criteria );

    if(empty($results_data) || !isset($results_data['field_data'])) return [];

    return $results_data;
  };


  /**
   * Prepare multiple row field data
   *
   * @param array $inputs
   * @param array $results_field_data
   *
   * @return array $results_field_data
   */
  $plugin -> prepare_survey_likert_multirow_data = function($inputs = [], $results_field_data = [] ) use ($plugin) {

    if (empty($inputs) || empty($results_field_data)) return [];

    // repack inputs
    $i=0;
    foreach($inputs as &$input){
      unset($inputs[$i]);
      $inputs[$input['name']] = $input;
      $i++;
    }

    //prepare field data
    foreach($results_field_data as $f_id => &$fd){
      foreach($fd as $k => &$v){
        if(isset($inputs[$k])){
          $nk = $inputs[$k]['label'];
          unset($fd[$k]);
          $fd[$nk] = $v;
        }
      }
    }

    return $results_field_data;
  };


  /**
   * Prepare survey likert type datasets
   *
   * @param object $form
   * @param array $results_field_data
   * @param array $inputs
   * @param bool $multirow
   *
   * @return array $chart_datasets
   */
  $plugin -> survey_likert_datasets = function($form = null, $results_field_data = [], $inputs=[], $multirow= false ) use ($plugin) {

    if (empty($form) || empty($results_field_data)) return [];

    if($multirow) {

      $results_field_data = $plugin -> prepare_survey_likert_multirow_data ($inputs, $results_field_data );

      foreach($results_field_data as $f_id => $fd) {
        foreach ( $fd as $k => $val ) {
          $data = [];
          if ( $k != 'sum_of_scores' ) {
            foreach ( $val as $kv => &$v ) {
              if ( $kv != 'row_score_sum' ) {
                $data[] = (int) $v;
              }
            }
            $chart_datasets[] = [ 'label' => $k,'data'  => $data ];
          }
        }
      }
    } else{
      $chart_datasets =  $plugin -> survey_datasets($form, $results_field_data );
    }

    return $chart_datasets;
  };


  /**
   * Prepare survey  datasets
   *
   * @param object $form
   * @param array $results_field_data
   * @param array $inputs
   * @param bool $multirow
   *
   * @return array $chart_datasets
   */
  $plugin -> survey_datasets = function($form = null, $results_field_data = [] ) use ($plugin) {
    foreach($results_field_data as $f_id => $fd) {
      $field = \GFAPI ::get_field( $form, $f_id );
      $data=[];
      foreach($fd as $k => $val ){
        if($k != 'sum_of_scores'){
          $data[]=$val;
        }
      }
      $chart_datasets[] = ['label'=>$field->label, 'data' =>$data];

    }

    return $chart_datasets;
  };


  /**
   * Get Quiz Score/Grade frequencies Chart labels
   *
   * @param array $results_data
   * @param array $form
   * @param array $field_objects_array
   * @param array $frequency_to_display - 'score_frequencies','grade_frequencies'
   * @param array $return - 'labels' or 'data'
   *
   * @return array $results
   */
  $plugin -> get_quiz_score_grade_data = function( $results_data=[], $form =[], $field_objects_array=[], $frequency_to_display = '', $return='data' ) use ($plugin) {

    if(empty($results_data) || empty($form) || empty($field_objects_array) || empty($frequency_to_display) ) return [];

    $results =[];

    $GFQuiz = new GFQuiz();
    $entries = \GFAPI::get_entries( $form['id'] );

    $quiz_results_calculation = $GFQuiz -> results_calculation( $results_data, $form, $field_objects_array, $entries );
    $quiz_frequencies = $quiz_results_calculation[$frequency_to_display];

    foreach ( $quiz_frequencies as $idx => $sf ) {
      if($return == 'labels') {
        $results[] = $idx;
      } elseif($return == 'data'){
        $results[]= $sf;
      }
    }

    if($return == 'data'){
      $l = $frequency_to_display == 'score_frequencies'? 'Score' : 'Grade' ;
      $results = ['label'=>$l.' Frequencies', 'data' =>$results];
    }

    return $results;
  };


  /**
   * Get labels
   *
   * @param array $field_objects_array
   * @param bool $multi_labels
   *
   * @return array $labels
   */
  $plugin -> get_labels = function( $field_objects_array = [], $multi_labels = false ) use ($plugin) {
    if(empty($field_objects_array)) return [];

    $labels =[];
    $labels_arr =[];

    if(!$multi_labels) {
      foreach ( $field_objects_array[0] -> choices as $choice ) {
        $labels[] = $choice[ 'text' ];
      }

    }else {

      // labels array from fields choices
      $i = 0;
      foreach ( $field_objects_array as $fo ) {
        foreach ( $fo -> choices as $choice ) {
          $labels_arr[ $i ][] = $choice[ 'text' ];
        }
        $i++;
      }

      // labels from labels array
      $labels = [];
      foreach ( $labels_arr as $la ) {
        $j = 0;
        foreach ( $la as $l ) {
          $labels[ $j ][] = $l;
          $j++;
        }
      }
    }

    return $labels;
  };


  /**
   * Get filtered field ids
   *
   * @param array $field_ids
   * @param object $form
   * @param bool $objects - whether to return objects array
   *
   * @return array $field_ids/$field_objects_array - filtered fields
   */
  $plugin -> get_filtered_fields = function( $field_ids = [], $form = null, $objects = false ) use ($plugin) {

    if(empty($field_ids) || empty($form)) return [];

    // all fields should be of the same type - the first field in array is referent
    $field_type = '';
    $field_input_type = '';
    $field_likert_type = false;

    $field_ids = array_values( array_filter( $field_ids, function ( $field_id, $idx ) use ( $plugin, $form, &$field_type, &$field_input_type, &$field_likert_type ) {

      $field = \GFAPI ::get_field( $form, $field_id );
      if ( empty( $field ) ) return false;

      if ( $idx == 0 ) {
        $field_type = $field -> type;
        $field_input_type = $field -> inputType;

        // additionally for survey/likert we need to distinguish between likert 'single row' and  likert 'multiple row' types
        if ( $field_input_type == 'likert' )  $field_likert_type = $field -> gsurveyLikertEnableMultipleRows;
      }

      if ( !in_array( $field_type, [ 'survey', 'quiz',  'poll' ] ) )  return false;

      $likert_condition = ( $field_input_type == 'likert' ) ? $field_likert_type == $field -> gsurveyLikertEnableMultipleRows : true;

      // for likert multi row type only one field allowed
      if($idx >= 1 && $field_input_type == 'likert' && $field -> gsurveyLikertEnableMultipleRows) return false;

      // make exeption for radio, select, checkbox input types - they can be shown/mixed together, on the same Chart
      $field_input_type_condition = (($field -> type == 'survey'|| $field -> type == 'quiz'|| $field -> type == 'poll') && ($field -> inputType == 'radio' || $field -> inputType == 'checkbox' || $field -> inputType == 'select' )) ? true : $field_input_type == $field -> inputType;

      return $likert_condition && $field_type == $field -> type && $field_input_type_condition;

    }, ARRAY_FILTER_USE_BOTH ) );

    if(!empty($objects)){
      $field_objects_array =[];
      foreach($field_ids as $f_id) {
        $f = \GFAPI ::get_field( $form, $f_id);
        $field_objects_array[]=$f;
      }

      //tgbl() -> see($field_objects_array);

      return $field_objects_array;
    }

    return $field_ids;
  };

