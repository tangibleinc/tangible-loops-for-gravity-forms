<?php

$plugin->register_settings([
  'css' => $plugin->assets_url . '/build/admin.min.css',
  'title_callback' => function() use ($plugin) {
    ?>
      <img class="plugin-logo"
        src="<?= $plugin->assets_url ?>/images/loops-and-logic-logo.png"
        alt="Tangible Loops & Logic Logo"
        width="40"
      >
      <?= $plugin->title ?>
    <?php
  },
]);
