<?php

  $plugin -> templates_path = function ($templates_dir_path, $files_dir = '' ) {
    if(empty($templates_dir_path)) return;
    if(!empty($files_dir)) $files_dir = $files_dir.'/';

    return $templates_dir_path.$files_dir;
  };


  $plugin -> dir_files = function ($path, $include_hidden = true, $recursive = true) use($plugin, $framework) {
    $files=[];
    if(empty($path) || empty($plugin) || empty($framework)) return $files;

    $filesystem = $framework->filesystem()->dirlist($path, $include_hidden,$recursive);

    foreach($filesystem as $fs_key => $fs_type){
      if($fs_type['type'] === 'd'){
        foreach($fs_type['files'] as $file => $data ){
          $files[]=$data;
        }
      } elseif($fs_type['type'] === 'f'){
        $files[]=$fs_type;
      }
    }

    return $files;
  };
