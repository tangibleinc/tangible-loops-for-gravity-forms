<?php
/*
 * Helper function to get array from array-like string
 *
 */
$plugin -> string_to_array = function($value) use ($plugin){

  $value = preg_replace('/(\[)|(\])/i', ' ', $value);
  $value = is_string($value)
    ? array_map( 'trim', explode( ',', $value ) )
    : (!is_array($value)
      ? [ $value ]
      : $value
    )
  ;
  foreach($value as &$v ){
    $v = trim(preg_replace('/(\'|\")|(\'|\")/i', ' ', $v));
  }
  return $value;
};

  /*
   * Get array from colors array-like string, e.g. "#bf2977, rgba(75, 192, 192, 0.5), rgb(75, 192, 192), hsla(9, 100%, 50%, 0.81), hsl(9, 100%, 50%)"
   *
   */
  $plugin -> colors_string_to_array = function($value = []) use ($plugin){

    $colors_array = [];
    if (empty($value)) return $colors_array;

    // reg ex - recognized color notations : hex(#), rgb, rgba, hsl, hsla
    $re = '/(#([\da-f]{3}){1,2}|(rgb|hsl)a\((\d{1,3}%?,\s?){3}(1|0|0?\.\d+)\)|(rgb|hsl)\(\d{1,3}%?(,\s?\d{1,3}%?){2}\))/';
    preg_match_all($re, $value, $matches,PREG_SET_ORDER);
    foreach($matches as $m ){
      $colors_array[] = $m[0];
    }

    return $colors_array;
  };

/*
 * Helper function to get boolean value
 *
 */
$plugin -> string_to_bool = function($value) use ($plugin){
  $value = trim($value);
  if( $value === 'true' || $value === 'false' ) {
    $value = $value === 'true';
  }else{
    $value = null;
  }
  return $value;
};

/*
 * Helper function add/remove loop fields prefix
 *
 */
$plugin -> field_prefix_handle = function($field_name, $prefix, $remove = false ) use ($plugin){

  $action = ($remove === true ) ? 'remove' : 'add';
  $f_n_arr = explode('_', $field_name );
  if($action==='add') {

    if ( $f_n_arr[0] !== $prefix ) {
      $field_name = $prefix . '_' . $field_name;
      return $field_name;
    }

  } else if($action=='remove'){

    $prefix_length = strlen($prefix.'_');
    $pos = strpos($field_name,$prefix . '_');

    if( $pos !== false && $pos == 0){
      $field_name = substr_replace( $field_name,'', 0,  $prefix_length);
      return $field_name;
    }
  }
  return $field_name;
};

  /*
   * Helper function if GF Add-On registered, eg. 'GFPolls', 'GFQuiz', 'GFSurvey' ...
   *
   */

  $plugin -> is_addon_active = function($addon = '' ) use ($plugin){
    $registered_addons = \GFAddOn::get_registered_addons();
    return !empty($addon) && in_array($addon,$registered_addons);
  };
