<?php

  /**
   * Insert a value or key/value pair after a specific key in an array.  If key doesn't exist, value is appended
   * to the end of the array.
   *
   * @param array $array  Array to insert into
   * @param string $key   Key after which to insert
   * @param array $new    Array with key/value pairs to insert
   *
   * @return array
   *
   * @see https://gist.github.com/wpscholar/0deadce1bbfa4adb4e4c
   */
  $plugin -> array_insert_after = function ( $array, $key, $new ) {
    $keys = array_keys( $array );
    $index = array_search( $key, $keys );
    $pos = false === $index ? count( $array ) : $index + 1;

    return array_merge( array_slice( $array, 0, $pos ), $new, array_slice( $array, $pos ) );
  };

  /**
   * Asc/desc sort associative array by children key
   *
   * @param array   $arr    Array to sort
   * @param string  $order  Order to sort - asc/desc
   * @param string  $key    Children key to sort by
   *
   * @return arr $arr Array sorted
   */
  $plugin -> array_uasort_asc_desc = function( $arr = [], $key = '' , $order = 'asc' ) {

    if( empty( $arr ) || !$key ) return $arr;
    $order = ( $order === 'asc' || $order === 'desc' ) ? $order : 'asc';

    uasort( $arr, function( $a, $b ) use( $key, $order ) {
      if ( $a[ $key ] == $b[ $key ] ) return 0;
      // asc
      if( $order == 'asc' ) {
        return ( $a[ $key ] < $b[ $key ] ) ? -1 : 1;
        // desc
      }elseif( $order == 'desc' ){
        return ( $a[ $key ] > $b[ $key ] ) ? -1 : 1;
      }
    });

    return $arr;
  };
