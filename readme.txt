=== Tangible: Loops for Gravity Forms ===
Requires at least: 4.0
Tested up to: 5.4.2
Requires PHP: 7
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags:

Loop types, fields, and conditions for Gravity Forms

== Description ==

Loop types, fields, and conditions for Gravity Forms

== Installation ==

1. Install & activate in the admin: *Plugins -&gt; Add New -&gt; Upload Plugins*

== Changelog ==

= 0.0.5 =

Release Date: 2021-07-05

- added GField tag for all Field Types

= 0.0.3 =

Release Date: 2021-06-15

- Data visualisation added
- Connect plugin to product on the store

= 0.0.2 =

Release Date: 2021-05-04

- Entry loop added
- Entry Fields loop added
- Examples added
- Tests added

= 0.0.1 =

Release Date: 2021-00-00

- Initial release
