/*
* Chart js
* @see https://www.chartjs.org
*
*/

var ChartJs = ($) => {

  if (!window.Tangible) return
  var ajax = window.Tangible.ajax
  var ajaxConfig = window.Tangible.ajaxConfig

  // Global object
  window.tgblChartJs = {}

  $(document).ready(function () {
    window.tgblChartJs.chartInit.constructor()
  })


  window.tgblChartJs.chartInit = {

    // initialize the code
    constructor: function () {

      // add the chart
      this.addChart();
    },


    addChart: function () {

      Chart.defaults.backgroundColor = 'rgba(11,168,239,0.5)'
      Chart.defaults.borderColor = 'rgba(11,168,239,1)'

      var chart_options = {}
      var chart_data_options = {}

      $('.tangible-chart').each(function() {

        let optionsTangibleChart = $(this).data('tangibleDynamicModuleOptions')
        let optionsTangibleChartData = $(this).find('.tangible-chart-data').data('tangibleDynamicModuleOptions')

        // chart

        if (optionsTangibleChart) {
          try {
            optionsTangibleChart = JSON.parse(optionsTangibleChart)
          } catch(e) { /* OK */ }
        }
        if (optionsTangibleChart && typeof optionsTangibleChart==='object' && !Array.isArray(optionsTangibleChart)) {
          // Valid options
          Object.assign(chart_options, optionsTangibleChart)
        }

        // chart data

        if (optionsTangibleChartData) {
          try {
            optionsTangibleChartData = JSON.parse(optionsTangibleChartData)
          } catch(e) { /* OK */ }
        }
        if (optionsTangibleChartData && typeof optionsTangibleChartData==='object' && !Array.isArray(optionsTangibleChartData)) {
          // Valid options
          Object.assign(chart_data_options, optionsTangibleChartData)
        }

        // canvas

        var ctx = $(this).find('.tgbl-chart');

        var type = String(chart_options.type)
        var fill = Boolean(chart_options.fill)
        var backgroundColor = chart_options.backgroundColor
        var borderColor = chart_options.borderColor
        var labels = chart_data_options.labels
        var datasets = chart_data_options.datasets

        if(datasets.length == 0) return;

        $.each(datasets, function( index, value ) {

          //if(typeof this !=='object') return;
          this['fill']=fill
          this['backgroundColor'] = backgroundColor
          this['borderColor'] = borderColor
          this['borderWidth'] = 1

          // cast colors for specific types
          if(type =='bar') {
            this['backgroundColor']= backgroundColor[index]
            this['backgroundColor'] = borderColor[index]
          }
        });

        console.log('datasets', datasets)
        console.log('chart_options', chart_options)
        console.log('chart_data_options',chart_data_options)

        var theChart = new Chart(ctx, {
          type: type,
          data: {
            labels: labels,
            datasets:datasets
          }
        });
      })

      // TODO remove test data below
      /*var ctx11 = document.getElementById('myChart');
      var myChart = new Chart(ctx11, {
        type: 'doughnut',
        //type: 'line',
        //type: 'bar',
        //type: 'radar',
        //type: 'pie',
        //type: 'polarArea',

        data: {
          labels: [
            'Eating',
            'Drinking',
            'Sleeping',
            'Designing',
            'Coding',
            'Cycling',
            'Running'
          ],
          datasets:
            [
              {
                label: 'My First Dataset',
                data: [65, 59, 90, 81, 56, 55, 40],
                fill: true,
                backgroundColor: ['rgba(255, 99, 132, 0.5)'],
                //backgroundColor: [
                  //'rgba(255, 99, 132, 0.5)',
                 // 'rgba(255, 159, 64, 0.5)',
                  //'rgba(255, 205, 86, 0.5)',
                  //'rgba(75, 192, 192, 0.5)',
                  //'rgba(54, 162, 235, 0.5)',
                  //'rgba(153, 102, 255, 0.5)',
                  //'rgba(10,81,224,0.5)'
                //],
                borderColor: ['rgb(226,11,54)'],
                //borderColor: [
                  //'rgb(226,11,54)',
                  //'rgb(255, 159, 64)',
                  //'rgb(255, 205, 86)',
                  //'rgb(75, 192, 192)',
                  //'rgb(54, 162, 235)',
                  //'rgb(153, 102, 255)',
                  //'rgb(4,72,208)'
                //],
                borderWidth: 1
              },
              {
                label: 'My Second Dataset',
                data: [28, 48, 40, 19, 96, 27, 100],
                fill: true,
                backgroundColor: 'rgba(83,236,17,0.5)',
                //backgroundColor: [
                  //'rgba(83,236,17,0.5)',
                  //'rgba(188,64,255,0.5)',
                  //'rgba(255,140,86,0.5)',
                  //'rgba(87,192,75,0.5)',
                  //'rgba(235,54,90,0.5)',
                  //'rgba(26,5,67,0.5)',
                  //'rgba(239,83,10,0.5)'
                //],
                borderColor: 'rgb(255, 99, 132)',
                //borderColor: [
                  //'rgb(255, 99, 132)',
                  //'rgb(255, 159, 64)',
                  //'rgb(255, 205, 86)',
                  //'rgb(75, 192, 192)',
                  //'rgb(54, 162, 235)',
                  //'rgb(153, 102, 255)',
                  //'rgb(8,79,221)'
                //],
                borderWidth: 1
              }
            ]
        },

      });*/
      //console.log(Chart.defaults)
    },
  }
}

export { ChartJs }

